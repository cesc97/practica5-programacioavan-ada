package anunci;

public class Anunci {
	private int durada;
	private float preu;

	public Anunci(int durada, float preu) {
		this.durada = durada;
		this.preu = preu;
	}

	public int getDurada() {
		return durada;
	}

	public void setDurada(int durada) {
		this.durada = durada;
	}

	public float getPreu() {
		return preu;
	}

	public void setPreu(float preu) {
		this.preu = preu;
	}

	@Override
	public String toString() {
		return "Anunci [durada=" + durada + ", preu=" + preu + "]";
	}

}
