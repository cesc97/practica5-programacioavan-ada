package anunci;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

public class Main {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		Anunci[] anuncis = null;
		ArrayList<Anunci> ordreFinal = new ArrayList<>();
		HashMap<Anunci, Float> anuncisMap = new HashMap<>();
		int segons = 0;
		System.out.println("quants anuncis vols generar?");
		int quants = scanner.nextInt();
		System.out.println("quants segons vols maxim?");
		int segonsMax = scanner.nextInt();
		System.out.println("els vols introduir a ma(m) o fer-los aleatoriament(a)?");
		char op = scanner.next().charAt(0);
		switch (op) {
		case 'a':
			anuncis = generarAnuncis(quants);
			break;
		case 'm':
			anuncis = anuncisAMa(quants);
			break;
		default:
			break;
		}

		for (int i = 0; i < anuncis.length; i++) {
			anuncisMap.put(anuncis[i], (float) 0);
		}
		for (int i = 1; i < anuncis.length; i++) {
			for (Anunci a : anuncisMap.keySet()) {
				anuncisMap.put(a, preuAnunci(a, i));
			}
			Anunci anunci = anunciMajorPreu(anuncisMap);
			if (anunci.getDurada()>segonsMax) {
				continue;
			}
			anuncisMap.remove(anunci);
			if (segonsMax <= segons) {
				break;
			}
			System.out.println(anunci + "--" + preuAnunci(anunci, i));
			System.out.println(anuncisMap.size());
			ordreFinal.add(anunci);
			segons += anunci.getDurada();

		}
		System.out.println("segons maxims:" + segonsMax);
		System.out.println("segons totals:" + segons);
		System.out.println("numero d'anuncis inicials:" + anuncis.length);
		System.out.println("numero anuncis:" + ordreFinal.size());
		System.out.println(ordreFinal.toString());

	}

	public static Anunci anunciMajorPreu(HashMap<Anunci, Float> map) {
		Anunci a = new Anunci(0, 0);
		float preu = preuAnunci(a, 0);
		for (Entry<Anunci, Float> entry : map.entrySet()) {
			Anunci key = entry.getKey();
			float value = entry.getValue();
			if (value > preu) {
				a = key;
				preu = value;
			}
		}

		return a;
	}

	public static Anunci[] generarAnuncis(int quants) {
		Anunci[] anuncis = new Anunci[quants];
		for (int i = 0; i < quants; i++) {
			anuncis[i] = generarAnunci();
		}
		return anuncis;
	}

	public static Anunci[] anuncisAMa(int quants) {
		Anunci[] anuncis = new Anunci[quants];
		for (int i = 0; i < quants; i++) {
			anuncis[i] = anunciAMa();
		}
		return anuncis;
	}

	private static Anunci anunciAMa() {
		Anunci a;
		System.out.println("durada");
		int durada = scanner.nextInt();
		scanner.reset();
		System.out.println("preu");
		float preu = scanner.nextFloat();
		a = new Anunci(durada, preu);
		return a;

	}

	private static Anunci generarAnunci() {
		Random r = new Random();
		int durada = (r.nextInt(300)) + 1;
		float preu = r.nextFloat() * 1000;
		Anunci anunci = new Anunci(durada, preu);
		return anunci;
	}

	public static float preuAnunci(Anunci anunci, int pos) {
		float preu = 0;
		preu = ((anunci.getPreu() * 1000) / (pos + 5000));
		return preu;
	}

}
/**
 * Quins s�n els candidats del problema? les diferents possibles ordenacions
 * dels anuncis Quina funci� de selecci� aplica la vostra soluci� de
 * programaci�? l'ordre dels anuncis La vostra funci� de selecci�, garanteix
 * trobar sempre la millor soluci�? Per qu�? no, necessitaria n! ordres
 * diferents pertal de trobar la millor de totes
 */
