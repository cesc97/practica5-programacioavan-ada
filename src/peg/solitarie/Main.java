package peg.solitarie;


// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {



	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		/*PegSolitarie pegSolitarie = new PegSolitarie();
		Solucio solu = new Solucio();
		System.out.println("Solving... ");
		if (pegSolitarie.trobarSolucio(pegSolitarie, solu)) {
			System.out.println(pegSolitarie.mostrarTaulell());
			solu.imprimirMoviements();
		} else {
			System.out.println("no hi ha solucio");
		}*/
		PegSolitarie pegSolitarie = new PegSolitarie();
		pegSolitarie.trobar2Solucions();
		Solucio[] solucions=pegSolitarie.getSolucions();
		for (int i = 0; i < solucions.length; i++) {
			solucions[i].imprimirMoviements();
		}
	}
}
