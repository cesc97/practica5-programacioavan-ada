package peg.solitarie;


// TODO: Auto-generated Javadoc
/**
 * The Class Moviment.
 */
public class Moviment {
	
	/** The inici. */
	private Coordenada inici;
	
	/** The fi. */
	private Coordenada fi;
	
	/** The menja. */
	private Coordenada menja;
	
	/** The direccio. */
	private int direccio = -1;
	
	/** The Constant amunt. */
	public static final int amunt = 0;
	
	/** The Constant avall. */
	public static final int avall = 1;
	
	/** The Constant dreta. */
	public static final int dreta = 2;
	
	/** The Constant esquerra. */
	public static final int esquerra = 3;

	/**
	 * Instantiates a new moviment.
	 *
	 * @param inici the inici
	 * @param fi the fi
	 * @param menja the menja
	 * @param direccio the direccio
	 */
	public Moviment(Coordenada inici, Coordenada fi, Coordenada menja, int direccio) {
		this.inici = inici;
		this.fi = fi;
		this.menja = menja;
		this.inici.setEstat(Coordenada.buida);
		this.menja.setEstat(Coordenada.buida);
		this.fi.setEstat(Coordenada.plena);
		this.direccio = direccio;
	}

	/**
	 * Gets the inici.
	 *
	 * @return the inici
	 */
	public Coordenada getInici() {
		return inici;
	}

	/**
	 * Sets the inici.
	 *
	 * @param inici the new inici
	 */
	public void setInici(Coordenada inici) {
		this.inici = inici;
	}

	/**
	 * Gets the fi.
	 *
	 * @return the fi
	 */
	public Coordenada getFi() {
		return fi;
	}

	/**
	 * Sets the fi.
	 *
	 * @param fi the new fi
	 */
	public void setFi(Coordenada fi) {
		this.fi = fi;
	}

	/**
	 * Gets the menja.
	 *
	 * @return the menja
	 */
	public Coordenada getMenja() {
		return menja;
	}

	/**
	 * Sets the menja.
	 *
	 * @param menja the new menja
	 */
	public void setMenja(Coordenada menja) {
		this.menja = menja;
	}

	/**
	 * Gets the direccio.
	 *
	 * @return the direccio
	 */
	public int getDireccio() {
		return direccio;
	}

	/**
	 * Sets the direccio.
	 *
	 * @param direccio the new direccio
	 */
	public void setDireccio(int direccio) {
		this.direccio = direccio;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Moviment [" + (inici != null ? "inici=" + inici + ", " : "") + (fi != null ? "fi=" + fi + ", " : "")
				+ (menja != null ? "menja=" + menja + ", " : "") + "direccio=" + direccio + "]";
	}

}
