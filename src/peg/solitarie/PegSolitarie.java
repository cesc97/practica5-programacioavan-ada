package peg.solitarie;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * The Class PegSolitarie.
 */
public class PegSolitarie {
	
	/** The taulell. */
	private Coordenada[][] taulell = new Coordenada[7][7];
	
	/** The solucions. */
	private Solucio[] solucions = new Solucio[2];

	/**
	 * Instantiates a new peg solitarie.
	 */
	public PegSolitarie() {
		generarTauelell();
	}

	/**
	 * Gets the taulell.
	 *
	 * @return the taulell
	 */
	public Coordenada[][] getTaulell() {
		return taulell;
	}

	/**
	 * Sets the taulell.
	 *
	 * @param taulell the new taulell
	 */
	public void setTaulell(Coordenada[][] taulell) {
		this.taulell = taulell;
	}

	/**
	 * Gets the solucions.
	 *
	 * @return the solucions
	 */
	public Solucio[] getSolucions() {
		return solucions;
	}

	/**
	 * Sets the solucions.
	 *
	 * @param solucions the new solucions
	 */
	public void setSolucions(Solucio[] solucions) {
		this.solucions = solucions;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "pegSolitarie [taulell=" + Arrays.toString(taulell) + ", solucions=" + Arrays.toString(solucions) + "]";
	}

	/**
	 * Generar tauelell.
	 */
	private void generarTauelell() {
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				if ((i < 2 || i > 4) && (j < 2 || j > 4)) {
					taulell[i][j] = new Coordenada(i, j, Coordenada.nula);
				} else {
					if ((i == 3) && (j == 3)) {
						taulell[i][j] = new Coordenada(i, j, Coordenada.buida);

					} else {
						taulell[i][j] = new Coordenada(i, j, Coordenada.plena);

					}
				}

			}
		}
	}

	/**
	 * Moure.
	 *
	 * @param coord the coord
	 * @param direccio the direccio
	 * @return the moviment
	 */
	public Moviment moure(Coordenada coord, int direccio) {
		Moviment moviment = null;
		switch (direccio) {
		case Moviment.amunt:
			if (potMoure(coord, direccio)) {
				moviment = new Moviment(coord, getCoodenada(coord.getFila() - 2, coord.getColumna()),
						getCoodenada(coord.getFila() - 1, coord.getColumna()), direccio);

			}
			break;

		case Moviment.avall:
			if (potMoure(coord, direccio)) {
				moviment = new Moviment(coord, getCoodenada(coord.getFila() + 2, coord.getColumna()),
						getCoodenada(coord.getFila() + 1, coord.getColumna()), direccio);
			}

			break;
		case Moviment.dreta:
			if (potMoure(coord, direccio)) {
				moviment = new Moviment(coord, getCoodenada(coord.getFila(), coord.getColumna() + 2),
						getCoodenada(coord.getFila(), coord.getColumna() + 1), direccio);
			}

			break;
		case Moviment.esquerra:
			if (potMoure(coord, direccio)) {
				moviment = new Moviment(coord, getCoodenada(coord.getFila(), coord.getColumna() - 2),
						getCoodenada(coord.getFila(), coord.getColumna() - 1), direccio);
			}
			break;
		}
		return moviment;
	}

	/**
	 * Gets the coodenada.
	 *
	 * @param fila the fila
	 * @param columna the columna
	 * @return the coodenada
	 */
	public Coordenada getCoodenada(int fila, int columna) {
		try {
			return taulell[fila][columna];
		} catch (Exception e) {
			// TODO: handle exception
		}
		//System.out.println(fila+"-"+columna);
		return new Coordenada(-1, -1, Coordenada.buida);

	}

	/**
	 * Pot moure.
	 *
	 * @param coord the coord
	 * @param direccio the direccio
	 * @return true, if successful
	 */
	public boolean potMoure(Coordenada coord, int direccio) {

		if ((coord.getColumna() < 2) && (coord.getFila() < 2)) {
			return false;
		} else if ((coord.getColumna() > 5) && (coord.getFila() > 5)) {
			return false;
		} else if ((coord.getColumna() < 2) && (coord.getFila() > 5)) {
			return false;
		} else if ((coord.getColumna() > 5) && (coord.getFila() < 2)) {
			return false;
		} else {

			switch (direccio) {
			case Moviment.amunt:
				if (coord.getFila() >= 2) {
					Coordenada da = getCoodenada(coord.getFila() - 1, coord.getColumna());
					Coordenada da2 = getCoodenada(coord.getFila() - 2, coord.getColumna());
					if ((da.getEstat().equals(Coordenada.plena)) && (da2.getEstat().equals(Coordenada.buida))) {
						return true;
					}
				}
				break;
			case Moviment.avall:
				if (coord.getFila() <= 5) {
					Coordenada b = getCoodenada(coord.getFila() + 1, coord.getColumna());
					Coordenada b2 = getCoodenada(coord.getFila() + 2, coord.getColumna());
					if ((b.getEstat().equals(Coordenada.plena)) && (b2.getEstat().equals(Coordenada.buida))) {
						return true;
					}
				}
				break;
			case Moviment.esquerra:
				if (coord.getColumna() >= 2) {
					Coordenada e = getCoodenada(coord.getFila(), coord.getColumna() - 1);
					Coordenada e2 = getCoodenada(coord.getFila(), coord.getColumna() - 2);
					if ((e.getEstat().equals(Coordenada.plena)) && (e2.getEstat().equals(Coordenada.buida))) {
						return true;
					}
				}
				break;
			case Moviment.dreta:
				if (coord.getColumna() <= 5) {
					Coordenada dr = getCoodenada(coord.getFila(), coord.getColumna() + 1);
					Coordenada dr2 = getCoodenada(coord.getFila(), coord.getColumna() + 2);
					if ((dr.getEstat().equals(Coordenada.plena)) && (dr2.getEstat().equals(Coordenada.buida))) {
						return true;
					}
				}
				break;

			default:
				break;
			}
		}
		return false;
	}

	/**
	 * Mostrar taulell.
	 *
	 * @return the string
	 */
	public String mostrarTaulell() {
		String tau = "";
		for (int i = 0; i < taulell.length; i++) {
			for (int j = 0; j < taulell.length; j++) {
				tau += taulell[i][j].imprimir();
				tau += "\t";
			}
			tau += "\n";
		}
		return tau;
	}
	
	/**
	 * Trobar solucio.
	 *
	 * @param pegSolitarie the peg solitarie
	 * @param solu the solu
	 * @param moviments the moviments
	 * @return true, if successful
	 */
	public boolean trobarSolucio(PegSolitarie pegSolitarie, Solucio solu) {
		if (solu.esSolucio(pegSolitarie.getTaulell())) {
			
			System.out.println("DONE");
			pegSolitarie.mostrarTaulell();
			return true;
		} else {
			for (int i = 0; i < 7; i++) {
				for (int j = 0; j < 7; j++) {
					for (int k = 0; k < 4; k++) {
						Coordenada coordenada = pegSolitarie.getCoodenada(i, j);
						if (pegSolitarie.potMoure(coordenada, k)) {

							Moviment m = pegSolitarie.moure(coordenada, k);
							System.out.println(pegSolitarie.mostrarTaulell());
							solu.afegirMoviment(m);

							if (trobarSolucio(pegSolitarie, solu)) {
								return true;
							} else {
								solu.desferMoviment();
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Trobar 2 solucions.
	 */
	public void trobar2Solucions() {
		Solucio solu0= new Solucio();
		boolean solucioTrobada=false;
		while (!solucioTrobada) {
			if (trobarSolucio(this, solu0)) {
				solucioTrobada=true;
				solucions[0]=solu0;
				
			}
		}
		
		Solucio solu1= new Solucio();
		solucioTrobada=false;
		while (!solucioTrobada) {
			if (trobarSolucio(this, solu1)) {
				solucioTrobada=true;
				solucions[1]=solu0;
				
			}
		}
	}
}
