package peg.solitarie;
/**
 * The Class Solucio.
 */

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class Solucio.
 */
public class Solucio {

	/** The moviments. */
	//private Moviment[] moviments = new Moviment[31];
	private ArrayList<Moviment> moviments= new ArrayList<>(); 

	/**
	 * Instantiates a new solucio.
	 */
	public Solucio() {
	}






	/**
	 * Gets the moviments.
	 *
	 * @return the moviments
	 */
	public ArrayList<Moviment> getMoviments() {
		return moviments;
	}



	/**
	 * Sets the moviments.
	 *
	 * @param moviments the new moviments
	 */
	public void setMoviments(ArrayList<Moviment> moviments) {
		this.moviments = moviments;
	}



	/**
	 * Afegir moviment.
	 *
	 * @param m the m
	 */
	public void afegirMoviment(Moviment m) {
		moviments.add(m);
	}
	/**
	 * Desfer moviment.
	 */
	public void desferMoviment() {
		Moviment m=moviments.get(moviments.size()-1);
		m.getInici().setEstat(Coordenada.plena);
		m.getMenja().setEstat(Coordenada.plena);
		m.getFi().setEstat(Coordenada.buida);
		moviments.remove(m);


	}

	/**
	 * Es solucio.
	 *
	 * @param taulell the taulell
	 * @return true, if successful
	 */
	public boolean esSolucio(Coordenada[][] taulell) {

		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				if ((taulell[3][3].getEstat() == Coordenada.plena)) {
					return false;
				} else if (!(taulell[i][j].getEstat() == Coordenada.buida)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Imprimir moviements.
	 */
	public void imprimirMoviements() {
		for (Moviment moviment : moviments) {
			System.out.println(moviment.toString());
		}


	}

}
