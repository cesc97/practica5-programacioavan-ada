package peg.solitarie;

// TODO: Auto-generated Javadoc
/**
 * The Class Coordenada.
 */
public class Coordenada {
	
	/** The fila. */
	private int fila;
	
	/** The columna. */
	private int columna;
	
	/** The estat. */
	private String estat;
	
	/** The Constant buida. */
	public static final String buida = "�";
	
	/** The Constant nula. */
	public static final String nula = " ";
	
	/** The Constant plena. */
	public static final String plena = "*";

	/**
	 * Instantiates a new coordenada.
	 *
	 * @param fila the fila
	 * @param columna the columna
	 * @param estat the estat
	 */
	public Coordenada(int fila, int columna, String estat) {
		super();
		this.fila = fila;
		this.columna = columna;
		this.estat = estat;
	}

	/**
	 * Gets the fila.
	 *
	 * @return the fila
	 */
	public int getFila() {
		return fila;
	}

	/**
	 * Sets the fila.
	 *
	 * @param fila the new fila
	 */
	public void setFila(int fila) {
		this.fila = fila;
	}

	/**
	 * Gets the columna.
	 *
	 * @return the columna
	 */
	public int getColumna() {
		return columna;
	}

	/**
	 * Sets the columna.
	 *
	 * @param columna the new columna
	 */
	public void setColumna(int columna) {
		this.columna = columna;
	}

	/**
	 * Gets the estat.
	 *
	 * @return the estat
	 */
	public String getEstat() {
		return estat;
	}

	/**
	 * Sets the estat.
	 *
	 * @param estat the new estat
	 */
	public void setEstat(String estat) {
		this.estat = estat;
	}

	/**
	 * Equals.
	 *
	 * @param anObject the an object
	 * @return true, if successful
	 */
	public boolean equals(Object anObject) {
		Coordenada c = (Coordenada) anObject;
		return this.estat.equals(c.estat);

	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Coordenada [fila=" + fila + ", columna=" + columna + ", " + (estat != null ? "estat=" + estat : "")
				+ "]";
	}

	/**
	 * Imprimir.
	 *
	 * @return the string
	 */
	public String imprimir() {
		return (estat != null ? estat : "");
	}
}
